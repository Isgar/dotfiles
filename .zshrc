export HISTSIZE=50000
export SAVEHIST=50000
setopt incappendhistory autocd nomatch notify histignorealldups histexpiredupsfirst nohup

# do not continue if non-interactive
[[ $- != *i* ]] && return

# use emacs keys
bindkey -e

# Farben!
autoload -U colors && colors
test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"

# termux chroot
if [ ! -e /bin ]; then
  termux-chroot && exit 0
fi

# fix completion with certain characters
ZLE_SPACE_SUFFIX_CHARS=$'&|'

# set EDITOR
if (which nvim > /dev/null); then
  export EDITOR=nvim
else
  export EDITOR=vim
fi

# de-pollute home dir
export XDG_CONFIG_HOME=$HOME/.config
export XDG_DATA_HOME=$HOME/.local/share
export XDG_CACHE_HOME=$HOME/.cache
export LESSHISTFILE=-
export VAGRANT_HOME="$XDG_DATA_HOME"/vagrant
export VAGRANT_ALIAS_FILE="$XDG_DATA_HOME"/vagrant/aliases
export PYLINTHOME="$XDG_CACHE_HOME"/pylint
export NPM_CONFIG_USERCONFIG=$XDG_CONFIG_HOME/npm/npmrc
export HISTFILE="$XDG_DATA_HOME"/zsh/history
export ANDROID_SDK_HOME="$XDG_CONFIG_HOME"/android
export ANDROID_AVD_HOME="$XDG_DATA_HOME"/android/
export ANDROID_EMULATOR_HOME="$XDG_DATA_HOME"/android/
export ADB_VENDOR_KEY="$XDG_CONFIG_HOME"/android

# load autocompletion
autoload -Uz compinit
mkdir -p $XDG_CACHE_HOME/zsh
mkdir -p $XDG_DATA_HOME/zsh
compinit -d $XDG_CACHE_HOME/zsh/zcompdump-$ZSH_VERSION

# source all configs in .zsh/
for config_file ($XDG_CONFIG_HOME/zsh/*.zsh) source $config_file

# add scripts commands if dir is set in private.zsh
[ ! -z "$SCRIPT_DIR" ] && export PATH=$PATH:$SCRIPT_DIR/main
[ ! -z "$SCRIPT_DIR" ] && alias log="$SCRIPT_DIR/main/log"

# fzf
if [ -e /usr/share/fzf/key-bindings.zsh ]; then
  source /usr/share/fzf/key-bindings.zsh
  source /usr/share/fzf/completion.zsh
  export FZF_DEFAULT_OPTS="--multi"
  export FZF_DEFAULT_COMMAND="fd -IH"
  export FZF_CTRL_T_COMMAND="fd -IH"
fi
