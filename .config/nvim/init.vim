" do not interpret stuff in files for security
set nomodeline

if $HOME == "/home"
  let s:termux = 1
endif

if executable('node')
  let s:node = 1
endif

" vim-plug
" auto-install
let s:plugpath = stdpath('data') . '/site/autoload/plug.vim'
let s:plugurl = "https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim"
if empty(glob(s:plugpath))
  silent exec "!curl -fLo " . s:plugpath . " --create-dirs " . s:plugurl
  echo "Downloaded plug.vim! Restart vim"
endif

" load plugs
call plug#begin(stdpath('data') . '/plugged')

" system
Plug 'roxma/vim-tmux-clipboard'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'gioele/vim-autoswap'
"Plug 'edkolev/tmuxline.vim'

" movement & visuals
Plug 'michaeljsmith/vim-indent-object'
Plug 'wellle/targets.vim'
Plug 'Yggdroot/indentLine'

" small commands
Plug 'tpope/vim-speeddating'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'
Plug 'lambdalisue/suda.vim'

" edit helpers
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-repeat'

" completion
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-nvim-lsp-signature-help'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'
Plug 'hrsh7th/cmp-omni'

" language specifics
Plug 'pearofducks/ansible-vim'
Plug 'iamcco/markdown-preview.nvim', { 'do': { -> mkdp#util#install() }, 'for': ['markdown', 'vim-plug']}
Plug 'dkarter/bullets.vim'
Plug 'lervag/wiki.vim'
Plug 'godlygeek/tabular'
Plug 'preservim/vim-markdown'
Plug 'chrisbra/csv.vim'

call plug#end()

" automatically install missing plugins and update them regularly
function! s:UpdatePlugged() abort
  let l:updatecheckfile = stdpath('data') . '/lastupdate'
  let l:update_plugs = len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
  if empty(glob(l:updatecheckfile))
    let l:update_plugs = 1
  elseif system('date -d now-30days +\%s') > system('cat ' . l:updatecheckfile)
    let l:update_plugs = 1
  endif
  if l:update_plugs
    PlugUpdate --sync | PlugUpgrade | q
    silent exec '!date "+\%s" > ' . l:updatecheckfile
  endif
endfunction
autocmd VimEnter * call s:UpdatePlugged()

set pastetoggle=<F2>
au BufRead,BufNewFile *.jsm set filetype=javascript
set scrolloff=4

" global clipboard as standard
" for nvim and wayland, need to install wl-clipboard
set clipboard=unnamedplus
" don't overwrite clipboard when pasting
vnoremap p "_dp

" Directories for swp files
set backupdir=$XDG_DATA_HOME/nvim

" remove .netrwhist
let netrw_dirhistmax = 0

" close buffers not visible
set nohidden

" activate touch screen for termux
if exists("s:termux")
  set mouse=a
  map <ScrollWheelUp> <C-Y>
  map <ScrollWheelDown> <C-E>
endif

" fix bad locale settings
scriptencoding utf-8
set encoding=utf-8

" indentation
set shiftwidth=2
set tabstop=2
set expandtab
set softtabstop=-1

" folding
set foldlevel=3
set foldmethod=syntax
au FileType python set foldmethod=indent
let sh_fold_enabled=7
highlight Folded ctermbg=black

" cursorline
set cursorline

" go out of vimdiff mode if closing a window
au WinEnter * call s:CloseDiffMode()
function s:CloseDiffMode()
  if (winnr('$') == 1)
    :diffoff
  endif
endfunction

" insert single chars with m, M
noremap <silent> m :exe "normal i".nr2char(getchar())<CR>
noremap <silent> M :exe "normal A".nr2char(getchar())<CR>
noremap t m

" highlight on yank
au TextYankPost * lua vim.highlight.on_yank {higroup="IncSearch", timeout=200, on_visual=true}

" leaders
let mapleader=","

" :DiffSaved, compare buffer to file on fs
function! s:DiffWithSaved()
  let filetype=&ft
  diffthis
  vnew | r # | normal! 1Gdd
  diffthis
  exe "setlocal bt=nofile bh=wipe nobl noswf ro ft=" . filetype
endfunction
com! DiffSaved call s:DiffWithSaved()

" insert current date and time
nmap <F3> a<C-R>=strftime("%Y-%m-%d")<CR><Esc>
imap <F3> <C-R>=strftime("%Y-%m-%d")<CR>
nmap <F4> a<C-R>=strftime("%H:%M")<CR><Esc>
imap <F4> <C-R>=strftime("%H:%M")<CR>

" airline
set noshowmode
set laststatus=2 " make it appear
let airline_powerline_fonts=1
let airline_theme = 'dark'

" bind fd and F1 to <esc>
ino fd <esc>
cno fd <c-c>
map <F1> <esc>
imap <F1> <esc>

" bind gh/gl to navigate tabs
noremap gh gT
noremap gl gt
noremap gt <NOP>
noremap gT <NOP>

" bind K to center cursor
noremap K M

" completely remove current file and quit
com! RmFile call delete(expand('%')) | q!

" overwrite unimpaired to find single entries in location list
command! Lnext try | lnext | catch | lfirst | catch | endtry
command! Lprev try | lprev | catch | llast | catch | endtry
noremap ]l :Lnext<CR>
noremap [l :Lprev<CR>

" indentLine
let indentLine_char = '⎸'

" Disable quote concealing in JSON files
let g:vim_json_conceal=0

" markdown
let bullets_outline_levels=[]
command! MD set filetype=markdown
au BufRead,BufNewFile 20[0-9][0-9]-[0-1][0-9]-[0-3][0-9] set filetype=markdown
au FileType markdown setl sw=2 sts=2 et
au FileType markdown setl indentkeys-=:
au FileType markdown let g:indentLine_setConceal = 0
au FileType markdown setl conceallevel=2
au FileType markdown setl concealcursor=n
" vim-markdown
let g:vim_markdown_folding_style_pythonic = 1
let g:vim_markdown_folding_level = 6
au FileType markdown map { <Plug>Markdown_MoveToPreviousHeader
au FileType markdown map } <Plug>Markdown_MoveToNextHeader
au FileType markdown map T :Toc<CR>

" completion
set completeopt=menu,menuone,noselect

lua <<EOF
-- supertab-like helper functions
local has_words_before = function()
  local line, col = unpack(vim.api.nvim_win_get_cursor(0))
  return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
end
local feedkey = function(key, mode)
  vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), mode, true)
end

-- Setup nvim-cmp and lspconfig
local cmp = require'cmp'

cmp.setup({
  snippet = {
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body)
    end,
  },
  window = {
    -- completion = cmp.config.window.bordered(),
    -- documentation = cmp.config.window.bordered(),
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-b>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<C-e>'] = cmp.mapping.abort(),
    ['<CR>'] = cmp.mapping.confirm({ select = false }),
    ["<Tab>"] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif vim.fn["vsnip#available"](1) == 1 then
        feedkey("<Plug>(vsnip-expand-or-jump)", "")
      elseif has_words_before() then
        cmp.complete()
      else
        fallback() -- The fallback function sends a already mapped key. In this case, it's probably `<Tab>`.
      end
    end, { "i", "s" }),
    ["<S-Tab>"] = cmp.mapping(function()
      if cmp.visible() then
        cmp.select_prev_item()
      elseif vim.fn["vsnip#jumpable"](-1) == 1 then
        feedkey("<Plug>(vsnip-jump-prev)", "")
      end
    end, { "i", "s" }),
  }),
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'nvim_lsp_signature_help' },
    { name = 'vsnip' },
    { name = 'path' },
    { name = 'omni' },
  }, {
    { name = 'buffer', option = { keyword_pattern = [[\k\+]] } }, -- option: match unicode/umlaut text
  })
})

-- Set configuration for specific filetype.
cmp.setup.filetype('gitcommit', {
  sources = cmp.config.sources({
    { name = 'cmp_git' },
  }, {
    { name = 'buffer' },
  })
})

cmp.setup.cmdline('/', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = {
    { name = 'buffer' }
  }
})

cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(),
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  })
})

-- lspconfig
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- sources: https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
local capabilities = require('cmp_nvim_lsp').default_capabilities(vim.lsp.protocol.make_client_capabilities())
require('lspconfig')['bashls'].setup { capabilities = capabilities }
require('lspconfig')['pyright'].setup { capabilities = capabilities }
EOF


" bullets
let g:bullets_enabled_file_types=['markdown', 'text', 'gitcommit']
let g:bullets_nested_checkboxes = 0

" archive note
com! ArchFile exec "!archive-note '".expand('%')."'"

" archive selection
function! s:archive_selection(mode) abort
  if getbufinfo('%')[0].changed
      echo "WARNING: need to save buffer before archiving possible"
      return
  endif
  if a:mode == "n"
    let l:line_start = line(".")
    let l:line_end = line(".")
  else
    let l:line_start = line("'<")
    let l:line_end = line("'>")
  endif
  let l:states = [".", "v", "'<", "'>"]
  " echo "start: ".line_start." | end: ".line_end
  let l:lines = getline(line_start, line_end)
  let l:tmpfile = "/tmp/nvim-archive-note"
  call writefile(lines, tmpfile)
  exec "!cat ".tmpfile." | archive-note --append '".expand('%')."'"
  if v:shell_error == 1
    return
  endif
  call delete(tmpfile)
  silent exec line_start.",".line_end."delete"
  write
  redraw | echo "archived ".len(lines)." lines"
endfunction
xmap <silent> <leader>A :<C-U>call <SID>archive_selection("v")<CR>
nmap <silent> <leader>A :call <SID>archive_selection("n")<CR>

" set top bar message
function! s:topbar_message()
  silent! normal! gv"my
  silent exec "!gsettings --schemadir $HOME/.local/share/gnome-shell/extensions/simple-message@freddez/schemas set org.gnome.shell.extensions.simple-message message " . shellescape(substitute(@m, "\n", "", "g"))
endfunction
vmap <silent> m :call <SID>topbar_message()<CR>

" wiki.vim
let g:wiki_root = '~/notes'
let g:wiki_ui_method = {
          \ 'confirm': 'nvim',
          \ 'input': 'nvim',
          \ 'select': 'nvim',
          \}

" csv.vim
nmap <silent> <leader>ca :1,$ArrangeColumn<CR>
nmap <silent> <leader>cA :1,$UnArrangeColumn<CR>
nmap <silent> <leader>cd :DeleteColumn<CR>
nmap <silent> <leader>ch :HiColumn<CR>
nmap <silent> <leader>cH :HiColumn!<CR>
nmap <silent> <leader>ct :Header<CR>
