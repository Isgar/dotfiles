# correct history search
if [ ! -z "$key" ]; then
  bindkey "${key[Up]}" history-beginning-search-backward
  bindkey "${key[Down]}" history-beginning-search-forward
else
  bindkey "^[[A" history-beginning-search-backward
  bindkey "^[[B" history-beginning-search-forward
fi

# disable C-s output suspense
stty -ixon

# shift-tab goes backwards in completion list
bindkey '^[[Z' reverse-menu-complete
