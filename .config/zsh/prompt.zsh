PROMPTSTART=${PROMPTSTART-'machine '}
PROMPTCOLOR1=${PROMPTCOLOR1-yellow}
PROMPTCOLOR2=${PROMPTCOLOR2-white}
PROMPTCOLOR3=${PROMPTCOLOR3-cyan}
PROMPTFINISH=${PROMPTFINISH-'>>'}

NNNPROMPT=
if [ -n "$NNNLVL" ]; then
  NNNPROMPT="n³"
fi

PROMPT='$NNNPROMPT$(gitprompt)%B%{$fg[$PROMPTCOLOR1]%}$PROMPTSTART%{$reset_color%}%B%{$fg[$PROMPTCOLOR2]%}%1~ %b%{$reset_color%}%{$fg[$PROMPTCOLOR3]%}%B$PROMPTFINISH%{$reset_color%} '
RPROMPT="[%{$fg_no_bold[$PROMPTCOLOR1]%}%?%{$reset_color%}]"
