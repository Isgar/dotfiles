# colors and ls sanity
alias ls='ls --color=auto'
alias grep='grep --color=auto'
alias ll='ls -alF'
alias lh='ls -AlFh'
alias la='ls -A'
alias l='ls -CF'

# aptitude
alias sui='sudo apt install'
alias sus='sudo apt search'
alias suup='sudo apt update && sudo apt upgrade'
## on termux
alias aptup='apt update && apt upgrade'
alias aptinst='apt install'
alias apts='apt search'

# pacman
if (which yay > /dev/null); then
  alias pacman=yay
fi
alias pacup='pacman -Syu'
alias pacs='pacman -Ss'
alias pacinst='pacman -S'
alias pacrm='pacman -Rc'
alias pacinstlocal='pacman -U'
alias paclistlocal='pacman -Qm'
alias pacinfo='pacman -Sii'
alias pacinfolocal='pacman -Qii'
alias paclistpurge='pacman -Qdtq'
alias pacfiles='pacman -Ql'
alias pacfileinfo='pacman -Qo'
alias pacpurge='pacman -Qdtq | pacman -Rs -'

# git
alias g='git'
alias gst='git status'
alias ga='git add'
alias gcam='git commit -am'
alias gcm='git commit -m'
alias gdi='git diff'
alias gps='git push'
alias gpl='git pull'
alias gch='git checkout'
alias gbr='git branch'

# neovim
if (which nvim > /dev/null); then
  alias vim='nvim'
  alias vimdiff='nvim -d'
fi

# note shortcuts
alias n='note'
ns() {
  if [ "$1" = "" ]; then
    return
  fi
  grep -isr "$1" "$NOTE_DIR"
  find "$NOTE_DIR/" -iname "*$1*" | grep -i "$1" || true
}
nsa() {
  if [ "$1" = "" ]; then
    return
  fi
  grep -isr "$1" "$ARCHIVE_DIR"
  find "$ARCHIVE_DIR/" -iname "*$1*" | grep -i "$1" || true
}

# misc
alias py='python3'
alias :q='exit'
alias freeram='sh -c "free && sync && echo 3 > /proc/sys/vm/drop_caches && free"'
alias list-extensions="find . -type f | sed 's/.*\.//' | grep -v / | sort | uniq -c"
alias psgrep='ps aux | grep'
alias fingrep='find | grep -i'
alias rg='rg --no-ignore'
alias fz='file="$(fzf)" && [ -f "$file" ] && xdg-open "$file"'
alias vz='file="$(fzf)" && [ -f "$file" ] && nvim "$file"'
